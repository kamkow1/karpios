#[macro_export]
macro_rules! meta_block {
    ($(#[$meta: meta] {$($item: item)*})*) => {
        $($(
            #[$meta]
            $item
        )*)*
    }
}
