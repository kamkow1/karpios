use core::fmt::Write;
use crate::{uart_print, uart_println};

#[no_mangle]
extern "C" fn kmain() {
    uart_println!("hello world");
}

