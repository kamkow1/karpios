use core::panic::PanicInfo;
use core::arch::asm;

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    unsafe { asm!("wfi") };
    loop { }
}

