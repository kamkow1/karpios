use core::fmt;
use crate::meta_block;

pub struct UartAddr(spin::Mutex<usize>);

meta_block! {
    #[cfg(feature = "machine_virt")] {
        pub static mut MACHINE_UART_BASE_ADDR: UartAddr = UartAddr(spin::Mutex::new(0x1000_0000));
    }
}

impl fmt::Write for UartAddr {
    fn write_str(&mut self, s: &str) -> Result<(), fmt::Error> {
        for &c in s.as_bytes() {
            uart_putc(c);
        }
        Ok(())
    }
}

pub fn uart_putc(c: u8) {
    unsafe {
        let ptr = (*MACHINE_UART_BASE_ADDR.0.lock()) as *mut u8;
        core::ptr::write_volatile(ptr, c);
    }
}

pub fn uart_getc() -> u8 {
    unsafe {
        let ptr = (*MACHINE_UART_BASE_ADDR.0.lock()) as *mut u8;
        core::ptr::read_volatile(ptr)
    }
}

#[macro_export]
macro_rules! uart_print {
	($($args:tt)+) => ({
        use core::fmt::Write;
        let _ = unsafe { write!(crate::uart::MACHINE_UART_BASE_ADDR, $($args)+) };
	});
}
#[macro_export]
macro_rules! uart_println {
	() => ({
		uart_print!("\r\n")
	});
	($fmt:expr) => ({
		uart_print!(concat!($fmt, "\r\n"))
	});
	($fmt:expr, $($args:tt)+) => ({
		uart_print!(concat!($fmt, "\r\n"), $($args)+)
	});
}
